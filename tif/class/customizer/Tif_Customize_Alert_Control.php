<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * TODO
 */
if ( ! function_exists( 'tif_customizer_alert' ) ) {

	add_action( 'customize_register', 'tif_customizer_alert' );

	function tif_customizer_alert( $wp_customize ) {

		if ( ! class_exists( 'WP_Customize_Control' ) )
			return null;

		class Tif_Customize_Alert_Control extends WP_Customize_Control {

			public function render_content() {

				if ( ! empty( $this->label ) )
					echo '<label class="customize-control-title tif-customizer-title">' . esc_html( $this->label ) . '</label>';

				if ( ! empty( $this->description ) )
					echo '<span class="customize-control-description tif-customizer-description' . ( isset ( $this->input_attrs['alert'] ) ? ' tif-customizer-' . esc_attr( $this->input_attrs['alert'] ) : null ) . '">' .  $this->description . '</span>';


			}

		}

	}

}
