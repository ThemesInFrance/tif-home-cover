<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * TODO
 */
if ( ! function_exists( 'tif_customizer_heading' ) ) {

	add_action( 'customize_register', 'tif_customizer_heading' );

	function tif_customizer_heading( $wp_customize ) {

		if ( ! class_exists( 'WP_Customize_Control' ) )
			return null;

		class Tif_Customize_Heading_Control extends WP_Customize_Control {

			public function render_content() {

				if ( ! empty( $this->label ) )
					echo '<label class="customize-control-title tif-customizer-title heading ' . ( isset ( $this->input_attrs['heading'] ) ? tif_sanitize_slug( $this->input_attrs['heading'] ) : 'title' ) . '"><span>' . esc_html( $this->label ) . '</span></label>';

				if ( ! empty( $this->description ) )
					echo '<span class="customize-control-description tif-customizer-description">' .  wp_kses( $this->description, tif_allowed_html() ) . '</span>';


			}

		}

	}

}
