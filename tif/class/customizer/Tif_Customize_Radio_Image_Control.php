<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * TODO
 */
if ( ! function_exists( 'tif_extend_image_radio_control' ) ) {

	add_action( 'customize_register', 'tif_extend_image_radio_control' );

	function tif_extend_image_radio_control( $wp_customize ) {

		if ( ! class_exists( 'WP_Customize_Control' ) )
			return null;

		class Tif_Customize_Radio_Image_Control extends WP_Customize_Control {

			public $type = 'tif-radio-image';

			public function render_content() {

				/* if no choices, bail. */
				if ( empty( $this->choices ) )
					return;

				$name = '_customize-' . $this->id;

				if ( ! empty( $this->label ) ) // add label if needed.
					echo '<label class="customize-control-title tif-customizer-title">' . esc_html( $this->label ) . '</label>';

				if ( ! empty( $this->description ) ) // add desc if needed.
					echo '<span class="customize-control-description tif-customizer-description">' .  wp_kses( $this->description, tif_allowed_html() ) . '</span>';

				?>

				<ul class="tif-radio-img">

				<?php

				foreach ( $this->choices as $value => $label ) :

					$src = $label;
					$legend = null;
					if ( is_array( $label ) ) {
						$src = $label[0];
						$legend  = (string)$label[1];
					}

					?>

					<li class="tif-radio-img-item">

						<input
						id="<?php echo esc_attr( $name ) . '_' . esc_attr( $value ); ?>"
						type="radio" value="<?php echo esc_attr( $value ); ?>"
						name="<?php echo esc_attr( $name ); ?>"
						class="tif-radio-img-input"

						<?php
						$this->link();
						checked( $this->value(), $value );
						?>
						/>

						<label for="<?php echo esc_attr( $name ) . '_' . esc_attr( $value ); ?>">

							<img src="<?php echo esc_url( $src ); ?>" />
							<?php if ( null != $legend ) echo '<small>' . esc_attr( $legend ) . '</small>' ?>

						</label>

					</li>

					<?php

				endforeach;

				?>

				</ul>

				<?php

			}

		}

	}

}
