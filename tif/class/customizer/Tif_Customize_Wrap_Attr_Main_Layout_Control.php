<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * TODO
 */
if ( ! function_exists( 'tif_wrap_attr_main_layout_control' ) ) {

	add_action( 'customize_register', 'tif_wrap_attr_main_layout_control' );

	function tif_wrap_attr_main_layout_control( $wp_customize ) {

		if ( ! class_exists( 'WP_Customize_Control' ) )
			return null;

		class Tif_Customize_Wrap_Attr_Main_Layout_Control extends WP_Customize_Control {

			public $type = 'tif-wrap-attr-main';

			public function render_content() {

				/* if no choices, bail. */
				if ( empty( $this->choices ) )
					return;

				$name = '_customize-' . $this->id;

				if ( ! empty( $this->label ) ) // add label if needed.
					echo '<label class="customize-control-title tif-customizer-title">' . esc_html( $this->label ) . '</label>';

				if ( ! empty( $this->description ) ) // add desc if needed.
					echo '<span class="customize-control-description tif-customizer-description">' .  wp_kses( $this->description, tif_allowed_html() ) . '</span>';

				?>

				<ul class="tif-wrap-loop-attr-control tif-radio-img">

				<?php

				$values       = tif_sanitize_multicheck( $this->value() );
				$hidden_value = is_array( $this->value() ) ? implode( ',', $this->value() ) : $this->value() ;

				foreach ( $this->choices as $key => $value ) {

					if( isset ( $value['id'] ) ) {

						$value = is_array($value) ? $value : array();

						if( $value['id'] == 'layout' ) {

							if ( isset ( $value['label'] ) && $value['label'] ) {
								echo '<strong class="tif-customizer-sub-title">' . esc_html( $value['label'] ) . '</strong>';
							} elseif( ! isset ( $value['label'] ) || $value['label'] != false ) {
								echo '<strong class="tif-customizer-sub-title">' . __( 'Layout', 'canopee' ) . '</strong>';
							}

							if ( isset ( $value['description'] ) && $value['description'] )
								echo sprintf( '<span class="tif-customizer-description">%s</span>',
								esc_html( $value['description'] )
							);

							foreach ( $value['choices'] as $k => $v ) :

								$src    = is_array( $v ) ? $v[0] : $v;
								$legend = is_array( $v ) ? (string)$v[1] : null;

								?>

								<li class="tif-radio-img-item">

									<input
									id="<?php echo esc_attr( $name ) . '_' . esc_attr( $k ); ?>"
									type="radio" value="<?php echo esc_attr( $k ); ?>"
									name="<?php echo esc_attr( $name ); ?>"
									class="layout"
									<?php
									checked( $values[$key], $k );
									?>
									/>

									<label for="<?php echo esc_attr( $name ) . '_' . esc_attr( $k ); ?>">

										<img src="<?php echo esc_url( $src ); ?>" />
										<?php if ( null != $legend ) echo '<small>' . esc_attr( $legend ) . '</small>' ?>

									</label>

								</li>

								<?php

							endforeach;

						}

						if( $value['id'] == 'post_per_line' ) {

							?>

							<li>
								<label>
									<?php

									if ( isset ( $value['label'] ) && $value['label'] ) {
										echo '<strong class="tif-customizer-sub-title">' . esc_html( $value['label'] ) . '</strong>';
									} elseif( ! isset ( $value['label'] ) || $value['label'] != false ) {
										echo '<strong class="tif-customizer-sub-title">' . __( 'How many items per line?', 'canopee' ) . '</strong>';
									}

									if ( isset ( $value['description'] ) && $value['description'] )
										echo sprintf( '<span class="tif-customizer-description">%s</span>',
										esc_html( $value['description'] )
									);

									?>
									<input type="number" class="post-per-line" value="<?php echo ( isset ( $values[$key] ) ? (int)$values[$key] : 1 ); ?>" min="1" max="4" />
								</label>
							</li>

							<?php

						}

						if( $value['id'] == 'thumbnail_size' ) {

							$selected = isset ( $values[$key] ) ? (string)$values[$key] : 'tif_thumb_medium';

							?>

							<li>
								<label>
									<?php

									if ( isset ( $value['label'] ) && $value['label'] ) {
										echo '<strong class="tif-customizer-sub-title">' . esc_html( $value['label'] ) . '</strong>';
									} elseif( ! isset ( $value['label'] ) || $value['label'] != false ) {
										echo '<strong class="tif-customizer-sub-title">' . __( 'Thumbnail size', 'canopee' ) . '</strong>';
									}

									if ( isset ( $value['description'] ) && $value['description'] )
										echo sprintf( '<span class="tif-customizer-description">%s</span>',
										esc_html( $value['description'] )
									);

									?>
									<select class="thumbnail-size">
										<option value="tif_thumb_small" <?php selected( $selected, 'tif_thumb_small' ); ?>><?php _e( 'Small', 'canopee' ) ?></option>
										<option value="tif_thumb_medium" <?php selected( $selected, 'tif_thumb_medium' ); ?>><?php _e( 'Medium', 'canopee' ) ?></option>
										<option value="tif_thumb_large" <?php selected( $selected, 'tif_thumb_large' ); ?>><?php _e( 'Large', 'canopee' ) ?></option>
									</select>
								</label>
							</li>

						<?php

						}

						if( $value['id'] == 'thumbnail_ratio' ) {

							$selected = isset ( $values[$key] ) ? (float)$values[$key] : '4.3';

							?>

							<li>
								<label>
									<?php

									if ( isset ( $value['label'] ) && $value['label'] ) {
										echo '<strong class="tif-customizer-sub-title">' . esc_html( $value['label'] ) . '</strong>';
									} elseif( ! isset ( $value['label'] ) || $value['label'] != false ) {
										echo '<strong class="tif-customizer-sub-title">' . __( 'Thumbnail ratio', 'canopee' ) . '</strong>';
									}

									if ( isset ( $value['description'] ) && $value['description'] )
										echo sprintf( '<span class="tif-customizer-description">%s</span>',
										esc_html( $value['description'] )
									);
								?>
								<select class="thumbnail-ratio">
									<option value="3.4" <?php selected( $selected, '3.4' ); ?>><?php _e( '3:4', 'canopee' ) ?></option>
									<option value="2.3" <?php selected( $selected, '2.3' ); ?>><?php _e( '2:3', 'canopee' ) ?></option>
									<option value="3.5" <?php selected( $selected, '3.5' ); ?>><?php _e( '3:5', 'canopee' ) ?></option>
									<option value="9.16" <?php selected( $selected, '9.16' ); ?>><?php _e( '9:16', 'canopee' ) ?></option>
									<option value="1.1" <?php  selected( $selected, '1.1' ); ?>><?php  _e( '1:1', 'canopee' ) ?></option>
									<option value="6.5" <?php  selected( $selected, '6.5' ); ?>><?php  _e( '6:5', 'canopee' ) ?></option>
									<option value="4.3" <?php  selected( $selected, '4.3' ); ?>><?php  _e( '4:3', 'canopee' ) ?></option>
									<option value="3.2" <?php  selected( $selected, '3.2' ); ?>><?php  _e( '3:2', 'canopee' ) ?></option>
									<option value="5.3" <?php  selected( $selected, '5.3' ); ?>><?php  _e( '5:3', 'canopee' ) ?></option>
									<option value="16.9" <?php selected( $selected, '16.9' ); ?>><?php _e( '16:9', 'canopee' ) ?></option>
									<option value="2.1" <?php  selected( $selected, '2.1' ); ?>><?php  _e( '2:1', 'canopee' ) ?></option>
									<option value="3.1" <?php  selected( $selected, '3.1' ); ?>><?php  _e( '3:1', 'canopee' ) ?></option>
									<option value="4.1" <?php  selected( $selected, '4.1' ); ?>><?php  _e( '4:1', 'canopee' ) ?></option>
								</select>
							</label>
						</li>

						<?php

						}

						if( $value['id'] == 'title' ) {

							?>

							<li>
								<label>
									<?php

									if ( isset ( $value['label'] ) && $value['label'] ) {
										echo '<strong class="tif-customizer-sub-title">' . esc_html( $value['label'] ) . '</strong>';
									} elseif( ! isset ( $value['label'] ) || $value['label'] != false ) {
										echo '<strong class="tif-customizer-sub-title">' . __( 'Title Loop', 'canopee' ) . '</strong>';
									}
									if ( isset ( $value['description'] ) && $value['description'] )
										echo sprintf( '<span class="tif-customizer-description">%s</span>',
										esc_html( $value['description'] )
									);

									?>
								<input class="title" type="text" value="<?php echo esc_attr( ( isset ( $values[$key] ) ? (string)$values[$key] : null ) ); ?>" />
							</label>
						</li>

						<?php

						}

						if( $value['id'] == 'title_tag' ) {

							$selected = isset ( $values[$key] ) ? (string)$values[$key] : 'h2';

							?>

							<li>
								<label>
									<?php

									if ( isset ( $value['label'] ) && $value['label'] ) {
										echo '<strong class="tif-customizer-sub-title">' . esc_html( $value['label'] ) . '</strong>';
									} elseif( ! isset ( $value['label'] ) || $value['label'] != false ) {
										echo '<strong class="tif-customizer-sub-title">' . __( 'Html title tag', 'canopee' ) . '</strong>';
									}

									if ( isset ( $value['description'] ) && $value['description'] )
										echo sprintf( '<span class="tif-customizer-description">%s</span>',
										esc_html( $value['description'] )
									);

									?>

									<select class="title-tag">
										<option value="h1" <?php selected( $selected, 'h1' ); ?>><?php _e( 'h1', 'canopee' ) ?></option>
										<option value="h2" <?php selected( $selected, 'h2' ); ?>><?php _e( 'h2', 'canopee' ) ?></option>
										<option value="h3" <?php selected( $selected, 'h3' ); ?>><?php _e( 'h3', 'canopee' ) ?></option>
										<option value="h4" <?php selected( $selected, 'h4' ); ?>><?php _e( 'h4', 'canopee' ) ?></option>
										<option value="h5" <?php selected( $selected, 'h5' ); ?>><?php _e( 'h5', 'canopee' ) ?></option>
										<option value="h6" <?php selected( $selected, 'h6' ); ?>><?php _e( 'h6', 'canopee' ) ?></option>
										<option value="div" <?php selected( $selected, 'div' ); ?>><?php _e( 'div', 'canopee' ) ?></option>
										<option value="p" <?php selected( $selected, 'p' ); ?>><?php _e( 'p', 'canopee' ) ?></option>
										<option value="span" <?php selected( $selected, 'span' ); ?>><?php _e( 'span', 'canopee' ) ?></option>
										<option value="strong" <?php selected( $selected, 'strong' ); ?>><?php _e( 'strong', 'canopee' ) ?></option>
										<option value="i" <?php selected( $selected, 'i' ); ?>><?php _e( 'i', 'canopee' ) ?></option>
									</select>
								</label>
							</li>

						<?php

						}

						if( $value['id'] == 'title_class' ) {

							?>

							<li>
								<label>
									<?php

									if ( isset ( $value['label'] ) && $value['label'] ) {
										echo '<strong class="tif-customizer-sub-title">' . esc_html( $value['label'] ) . '</strong>';
									} elseif( ! isset ( $value['label'] ) || $value['label'] != false ) {
										echo '<strong class="tif-customizer-sub-title">' . __( 'Title additional CSS Class(es)', 'canopee' ) . '</strong>';
									}

									if ( isset ( $value['description'] ) && $value['description'] )
										echo sprintf( '<span class="tif-customizer-description">%s</span>',
										esc_html( $value['description'] )
									);

									?>
									<input class="title-class" type="text" value="<?php echo esc_attr( ( isset ( $values[$key] ) ? (string)$values[$key] : null ) ); ?>" />
								</label>
							</li>

							<?php

						}

						if( $value['id'] == 'container_class' ) {

							?>

							<li>
								<label>
									<?php

									if ( isset ( $value['label'] ) && $value['label'] ) {
										echo '<strong class="tif-customizer-sub-title">' . esc_html( $value['label'] ) . '</strong>';
									} elseif( ! isset ( $value['label'] ) || $value['label'] != false ) {
										echo '<strong class="tif-customizer-sub-title">' . __( 'Container additional CSS Class(es)', 'canopee' ) . '</strong>';
									}

									if ( isset ( $value['description'] ) && $value['description'] )
										echo sprintf( '<span class="tif-customizer-description">%s</span>',
										esc_html( $value['description'] )
									);

									?>
									<input class="container-class" type="text" value="<?php echo esc_attr( ( isset ( $values[$key] ) ? (string)$values[$key] : null ) ); ?>" />
								</label>
							</li>

							<?php

						}

						if( $value['id'] == 'post_class' ) {

							?>

							<li>
								<label>
									<?php

									if ( isset ( $value['label'] ) && $value['label'] ) {
										echo '<strong class="tif-customizer-sub-title">' . esc_html( $value['label'] ) . '</strong>';
									} elseif( ! isset ( $value['label'] ) || $value['label'] != false ) {
										echo '<strong class="tif-customizer-sub-title">' . __( 'Post additional CSS Class(es)', 'canopee' ) . '</strong>';
									}

									 if ( isset ( $value['description'] ) && $value['description'] )
										 echo sprintf( '<span class="tif-customizer-description">%s</span>',
										 esc_html( $value['description'] )
									 );

									?>
									<input class="post-class" type="text" value="<?php echo esc_attr( ( isset ( $values[$key] ) ? (string)$values[$key] : null ) ); ?>" />
								</label>
							</li>

							<?php

						}

					}

				}

				?>

				<input type="hidden" <?php $this->link(); ?> value="<?php echo esc_attr( $hidden_value ); ?>" />
				</ul>

				<?php
			}

		}

	}

}
