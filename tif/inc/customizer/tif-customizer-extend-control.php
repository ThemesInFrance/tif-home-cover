<?php

if ( ! defined( 'ABSPATH' ) ) exit;

require_once TIF_COMPONENTS_DIR . 'class/customizer/Tif_Customize_Color_Control.php';
require_once TIF_COMPONENTS_DIR . 'class/customizer/Tif_Customize_Category_Dropdown_Control.php';
require_once TIF_COMPONENTS_DIR . 'class/customizer/Tif_Customize_Checkbox_Multiple_Control.php';
require_once TIF_COMPONENTS_DIR . 'class/customizer/Tif_Customize_Checkbox_Sortable_Control.php';
require_once TIF_COMPONENTS_DIR . 'class/customizer/Tif_Customize_Image_Cover_Fit_Control.php';
require_once TIF_COMPONENTS_DIR . 'class/customizer/Tif_Customize_Number_Multiple_Control.php';
require_once TIF_COMPONENTS_DIR . 'class/customizer/Tif_Customize_Wrap_Attr_Main_Layout_Control.php';
require_once TIF_COMPONENTS_DIR . 'class/customizer/Tif_Customize_Wrap_Attr_Content_Control.php';
require_once TIF_COMPONENTS_DIR . 'class/customizer/Tif_Customize_Radio_Image_Control.php';
require_once TIF_COMPONENTS_DIR . 'class/customizer/Tif_Customize_Range_Multiple_Control.php';
require_once TIF_COMPONENTS_DIR . 'class/customizer/Tif_Customize_Select_Multiple_Control.php';
require_once TIF_COMPONENTS_DIR . 'class/customizer/Tif_Customize_Text_Editor_Control.php';
require_once TIF_COMPONENTS_DIR . 'class/customizer/Tif_Customize_Text_Sortable_Control.php';
require_once TIF_COMPONENTS_DIR . 'class/customizer/Tif_Customize_Box_Shadow_Control.php';
require_once TIF_COMPONENTS_DIR . 'class/customizer/Tif_Customize_Alert_Control.php';
require_once TIF_COMPONENTS_DIR . 'class/customizer/Tif_Customize_Heading_Control.php';
