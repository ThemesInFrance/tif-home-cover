<?php

if ( ! defined( 'ABSPATH' ) ) exit;

require_once( ABSPATH . 'wp-admin/includes/file.php' );

function tif_build_blank_thumbnail( $width = false, $height = false, $path = false ) {

	if ( ! $width || ! $height )
		return;

	if ( ! $path ) :
		$dir  = Themes_In_France::tif_theme_assets_dir();
		$path = $dir['basedir'] . '/assets/img/blank-' . intval($width) . 'x' . intval($height) . '.png';
	endif;

	if ( file_exists( $path ) )
		return;

	$im    = imagecreatetruecolor( intval($width), intval($height) );
	$black = imagecolorallocate( $im, 0, 0, 0 );

	imagecolortransparent( $im, $black );
	// header( 'Content-Type: image/png' );
	imagepng( $im, $path );
	imagedestroy( $im );

}
