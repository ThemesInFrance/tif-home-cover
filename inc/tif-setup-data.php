<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Get Home Cover setup data
 */

function tif_plugin_home_cover_setup_data() {

	$boxed_width = class_exists ( 'Themes_In_France' ) ? tif_get_option( 'theme_init', 'tif_width,primary', 'lenght' ) : '1200';

	return $tif_home_cover_setup_data = array(

		'tif_tabs'                  => 1,

		// Settings
		'tif_init'                  => array(
			'enabled'                   => true,
			'customizer_enabled'        => 1,
			'css_enabled'               => '',
			'generated'                 => array( null ),
			'capabilities'              => '',
			'custom_css'                => '',
		),

		// Layout
		'tif_home_cover_layout'     => array(
			'layout'                    => array(
				'cover',
			),
			'boxed_width'               => $boxed_width,
			'wide_alignment'            => 'alignfull',
			'cover_fit'                 => array(
				'cover',
				'center center',
				'70',
				'vh',
				1 // (bool) fixed
			),
			'box'                       => array(
				'padding'                   => 'spacer_medium_plus,spacer_medium_plus',
			),
			'alignment'                 => array(
				'align_items'               => 'start',
				'justify_content'           => 'center',
				'gap'                       => 'gap_large',
			),
		),

		// Layout
		'tif_home_cover_colors'     => array(
			'bgcolor'                   => '#ffffff,normal,1',
			'overlay_bgcolor'           => '#000000,normal,.5',
			'title_bgcolor'             => '#000000,normal,0',
			'subtitle_bgcolor'          => '#000000,normal,0',
			'excerpt_bgcolor'           => '#000000,normal,0',
			// 'content_bgcolor'           => '#000000,normal,.5',
			'title_color'               => '#ffffff',
			'subtitle_color'            => '#ffffff',
			'excerpt_color'             => '#ffffff',
		),

		'tif_home_cover_content'    => array(
			'cover_1'                   => array(

				'thumbnail_id'              => null,
				'link'                      => '#',
				'title'                     => array(
					esc_html__( 'Want to say Hey or find out more ?', 'tif-home-cover' ),
					'h2',
				),
				'subtitle'                  => array(
					esc_html__( 'The place is perfect', 'tif-home-cover' ),
					'h3',
				),
				'excerpt'                   => array(
					'Reprehen derit in voluptate velit cillum dolore eu fugiat nulla pariaturs sint occaecat proidentse.<br /><a href="https://unsplash.com/photos/KdeqA3aTnBY" target="_blank">Photo : Dylan Gillis / Unsplash</a>',
					'span'
				),

				'btn1_enabled'              => 1,
				'btn1_link_target'          => '',
				'btn1_style'                => 'btn--default btn-default',
				'btn1_text'                 => esc_html__( 'Purchase Now!', 'tif-home-cover' ),
				'btn1_link'                 => '#',

				'btn2_enabled'              => 1,
				'btn2_link_target'          => '',
				'btn2_style'                => 'btn--default btn-default',
				'btn2_text'                 => esc_html__( 'Get in Touch!', 'tif-home-cover' ),
				'btn2_link'                 => '#',
			)

		),

	);

}
