<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Home Cover front render function
 */
function tif_render_home_cover() {

	if ( is_paged() )
		return;

	if( null == tif_get_option( 'plugin_home_cover', 'tif_init,enabled', 'checkbox' ) ) {

		if ( current_user_can( 'customize' ) ) {

		?>

		<section id="tif-home-cover" class="tif-home-cover container">

			<div class="inner tif-boxed">

				<div class="tif-alert tif-alert--warning">

					<?php

					if ( is_customize_preview() ) :
						echo '<span id="tif-home-cover-disabled"></span>';
						printf(
							'<p>%s</p>',
							esc_html__( 'Home Cover is disabled.', 'tif-home-cover' )
						);
					endif;

					if ( ! is_customize_preview() )
					printf(
						'<p><a href="%2$s">%1$s</a>.</p>',
						esc_html__( 'Home Cover can be enabled in the customizer', 'tif-home-cover' ),
						admin_url( 'customize.php?autofocus[section]=tif_plugin_home_cover_panel_settings_section' )
					);

					?>

				</div>

			</div><!-- .inner -->

		</section><!-- #tif-home-cover -->

		<?php

		}

		return;

	}

	if( null == tif_get_option( 'plugin_home_cover', 'tif_init,enabled', 'checkbox' ) )
		return;

	$layout               = tif_get_option( 'plugin_home_cover', 'tif_home_cover_layout', 'array' );
	$content              = tif_get_option( 'plugin_home_cover', 'tif_home_cover_content,cover_1', 'array' );
	$colors               = tif_get_option( 'plugin_home_cover', 'tif_home_cover_colors', 'array' );
	$object               = tif_sanitize_cover_fit( $layout['cover_fit'] );

	$title_bgcolor        = $colors['title_bgcolor'];
	$title_bgcolor        = is_array( $title_bgcolor ) ? $title_bgcolor : explode( ',', $title_bgcolor );
	$title_has_bgcolor    = $title_bgcolor[2] > 0 ? ' has-background' : null;

	$subtitle_bgcolor     = $colors['subtitle_bgcolor'];
	$subtitle_bgcolor     = is_array( $subtitle_bgcolor ) ? $subtitle_bgcolor : explode( ',', $subtitle_bgcolor );
	$subtitle_has_bgcolor = $subtitle_bgcolor[2] > 0 ? ' has-background' : null;

	$excerpt_bgcolor      = $colors['excerpt_bgcolor'];
	$excerpt_bgcolor      = is_array( $excerpt_bgcolor ) ? $excerpt_bgcolor : explode( ',', $excerpt_bgcolor );
	$excerpt_has_bgcolor  = $excerpt_bgcolor[2] > 0 ? ' has-background' : null;

	$title                = tif_sanitize_wrap_attr( $content['title'] );
	$subtitle             = tif_sanitize_wrap_attr( $content['subtitle'] );
	$excerpt              = tif_sanitize_wrap_attr( $content['excerpt'] );
	$btn1_text            = (string)$content['btn1_text'];
	$btn2_text            = (string)$content['btn2_text'];
	$btn1_link            = esc_url_raw( $content['btn1_link'] );
	$btn2_link            = esc_url_raw( $content['btn2_link'] );

	$link                 = ! empty( $content['link'] ) ? $content['link'] : null;
	$link_open            = null != $link ? '<a href="' . esc_url_raw( $link ) . '">' : '';
	$link_close           = null != $link_open ? '</a>' : null ;

	$thumb_id             = isset( $content['thumbnail_id'] ) && null != $content['thumbnail_id'] ? (int)$content['thumbnail_id'] : false;

	$layout_attr          = tif_sanitize_multicheck( $layout['layout'] );
	$layout_attr          = tif_get_loop_attr(
		array(
			$layout_attr[0],
			2,
			null,
			null,
			null,
			null,
			( isset( $layout_attr[1] ) ? $layout_attr[1] : false ),
			( isset( $layout_attr[2] ) ? $layout_attr[2] : false ),
		),
		array(
			'has_overlay'    => true,
		)
	);

	$thumb_attr = array(
		'thumbnail'         => array(
			'size'              => 'tif-thumb-large',
			'id'                => ( $thumb_id ? (int)$thumb_id : false ),
			'url'               => ( ! $thumb_id ? esc_url( TIF_HOME_COVER_URL . 'assets/img/home-cover-1-bg.jpg' ) : false ),
			'object'            => array(
				'fit'               => $object[0],
				'position'          => $object[1],
				'height'            => tif_get_length_value( $object[2] . ',' . $object[3] ),
				'fixed'             => isset( $object[4] ) && $object[4] ? (bool)$object[4] : false,
			),
			'attr'              => array(
				'class'             => false,
				'style'             => false,
			),
		),
	);

	?>

	<section id="tif-home-cover" class="tif-home-cover">

		<div class="inner <?php echo ( null != $layout['wide_alignment'] ? tif_sanitize_css( $layout['wide_alignment'] ) : false ); ?>">

			<article <?php echo tif_parse_attr( $layout_attr['container']['attr'] ) ?>>

				<?php

				// Open Inner if there is one
				if ( isset( $layout_attr['inner']['wrap'] ) && $layout_attr['inner']['wrap'] )
					echo '<' . esc_attr( $layout_attr['inner']['wrap'] ) . tif_parse_attr( $layout_attr['inner']['attr'] ) . '>' . "\n\n" ;

				echo tif_attachment_thumbnail( $thumb_attr );

				?>

				<div <?php echo tif_parse_attr( $layout_attr['post']['wrap_content']['attr'] ) ?>>

					<?php

					do_action( 'tif.plugin_home_cover.before' );

					if ( null != $title[0] || null != $subtitle[0] || null != $excerpt )
						echo '<header>';

					if( null != $title[0] ){

						echo '<' . esc_attr( $title[1] ) . ' class="entry-title cover-title ' .
							esc_attr( isset( $title[2] ) && $title[2] ? $title[2] : null ) .
							'"><span ' . tif_parse_attr( array( 'class' => $title_has_bgcolor ) ) . '>';
						echo $link_open.  wp_kses( $title[0], tif_allowed_html() ) . $link_close;
						echo '</span></' . esc_attr( $title[1] ) . '>';

						do_action( 'tif.plugin_home_cover.after.title' );

					}

					if( null != $subtitle[0] ){

						echo '<' . esc_attr( $subtitle[1] ) . ' class="entry-sub-title cover-sub-title ' .
							esc_attr( isset( $subtitle[2] ) && $subtitle[2] ? $subtitle[2] : null ) .
							'"><span ' . tif_parse_attr( array( 'class' => $subtitle_has_bgcolor ) ) . '>';
						echo wp_kses( $subtitle[0], tif_allowed_html() );
						echo '</span></' . esc_attr( $subtitle[1] ) . '>';

						do_action( 'tif.plugin_home_cover.after.subtitle' );

					}

					if( null != $excerpt[0] ){

						echo '<' . esc_attr( $excerpt[1] ) . ' class="entry-content ' . $excerpt_has_bgcolor .
							esc_attr( isset( $excerpt[2] ) && $excerpt[2] ? $excerpt[2] : null ) .
							'">';
						echo wp_kses( $excerpt[0], tif_allowed_html() );
						echo '</' . esc_attr( $excerpt[1] ) . '>';

						do_action( 'tif.plugin_home_cover.after.excerpt' );

					}

					if ( null != $title[0] || null != $subtitle[0] || null != $excerpt )
						echo '</header>';


					if( $content['btn1_enabled'] || $content['btn2_enabled'] )
						echo '<footer ' . tif_parse_attr( array( 'class' => 'flex gap-5' ) ) . '>';

						if ( $content['btn1_enabled'] )
							echo '<a href="' . esc_url_raw( $btn1_link ) . '"
								' . ( $content['btn1_link_target'] == 1 ? 'target="_blank" rel="noreferrer noopener"' : null ) . '
								class="btn ' . esc_attr( $content['btn1_style'] ) . '">
								' . esc_html( $btn1_text ) . '
							</a>';

						if ( $content['btn2_enabled'] )
							echo '<a href="' . esc_url_raw( $btn2_link ) . '"
								' . ( $content['btn2_link_target'] == 1 ? 'target="_blank" rel="noreferrer noopener"' : null ) . '
								class="btn ' . esc_attr( $content['btn2_style'] ) . '">
								' . esc_html( $btn2_text ) . '
							</a>';

						do_action( 'tif.plugin_home_cover.after.button' );

					if( $content['btn1_enabled'] || $content['btn2_enabled'] )
						echo '</footer>';


					do_action( 'tif.plugin_home_cover.after' );

					?>

				</div>

				<?php

				// Close Inner if there is one
				if ( isset( $layout_attr['inner']['wrap'] ) && $layout_attr['inner']['wrap'] )
 					echo '</' . esc_attr( $layout_attr['post']['inner']['wrap'] ) . '>' . "\n\n" ;

				?>

			</article>

		</div><!-- .inner -->

	</section><!-- #tif-home-cover -->

	<?php

}
