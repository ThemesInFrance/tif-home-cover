<?php

if ( ! defined( 'ABSPATH' ) ) exit;

function tif_home_cover_options_page() {

	$capability = tif_get_submenu_capability( tif_get_option( 'plugin_home_cover', 'tif_init,capabilities', 'multicheck' ) ) ;

	if ( ! current_user_can( $capability ) )
		wp_die( esc_html__( 'Unauthorized user.', 'tif-home-cover' ) );

	?>

	<div class="wrap">

		<h1 class="tif-plugin-title"><?php _e( 'Home Cover', 'tif-home-cover' ) ?></h1>
		<p><?php _e( 'Create a beautiful call to action section in minutes on your homepage.', 'tif-home-cover' ) ?></p>

		<form method="post" action="options.php" class="tif-form tif-plugin-metabox row tif-tabs">

			<?php

			settings_fields( 'tif-home-cover' );
			// do_settings_sections( 'tif-home-cover' );
			wp_nonce_field( 'tif_home_cover_action', 'tif_home_cover_nonce_field' );
			$tif_plugin_name = 'tif_plugin_home_cover';

			// Create a new instance
			// Pass in a URL to set the action
			$form = new Tif_Form_Builder();

			// Form attributes are modified with the set_att function.
			// First argument is the setting
			// Second argument is the value
			$count = 0;
			$tabs = 0;

			$form->set_att( 'method', 'post' );
			$form->set_att( 'enctype', 'multipart/form-data' );
			$form->set_att( 'markup', 'html' );
			$form->set_att( 'class', 'tif-form tif-tabs' );
			// $form->set_att( 'id', 'megapouet' );
			$form->set_att( 'novalidate', false );
			// $form->set_att( 'action', 'options.php' );
			// $form->set_att( 'add_honeypot', false );
			// $form->set_att( 'add_nonce', 'tif_home_cover' );
			$form->set_att( 'is_array', 'tif_plugin_home_cover' );
			$form->set_att( 'form_element', false );
			$form->set_att( 'add_submit', true );

			// Uss add_input to create form fields
			// First argument is the name
			// Second argument is an array of arguments for the field
			// Third argument is an alternative name field, if needed

			/**
			 * New tab for administrator settings
			 */
			$form->add_input( 'tabs' . $tabs++, array(
				'type' => 'tabs',
				'id' => 'tab-' . $tabs,
				'value' => $tabs,
				'checked' => esc_attr( tif_get_option( 'plugin_home_cover', 'tif_tabs', 'int' ) ),
				'label' => esc_html__( 'Settings', 'tif-home-cover' ),
				'dashicon' => 'dashicons-admin-settings',
				'first' => true,
			) );

			$form->add_input( 'html' . $count++ , array(
				'type' => 'html',
				'value' => '<fieldset>'."\n".'<legend>' . esc_html__( 'My Settings legend', 'tif-home-cover' ) . '</legend>'
			) );

				require_once 'tif-settings-tab-settings.php';

			$form->add_input( 'html' . $count++, array(
				'type' => 'html',
				'value' => '</fieldset>'
			) );

			/**
			 * New tab for help
			 */
			$form->add_input( 'tabs' . $tabs++, array(
				'type' => 'tabs',
				'id' => 'tab-' . $tabs,
				'value' => $tabs,
				'checked' => esc_attr( tif_get_option( 'plugin_home_cover', 'tif_tabs', 'int' ) ),
				'label' => esc_html__( 'Help', 'tif-home-cover' ),
				'dashicon' => 'dashicons-editor-help',
				'wrap_tag' => '',
				'before_html' => '</div>',
			) );

			$form->add_input( 'html' . $count++ , array(
				'type' => 'html',
				'value' => '<div class="tif-help"><fieldset>'."\n".'<legend>' . esc_html__( 'Help', 'tif-home-cover' ) . '</legend>'
			) );

				require_once 'tif-settings-tab-help.php';

			$form->add_input( 'html' . $count++, array(
				'type' => 'html',
				'value' => '</fieldset></div>'
			) );

			$form->add_input( 'html' . $count++, array(
				'type' => 'html',
				'value' => '</div>',
			) );

			// Create the form
			$form->build_form();

			?>

		</form>

	</div><!-- .wrap -->

	<?php

	// echo wp_kses( tif_plugin_powered_by(), tif_allowed_html() );
	echo wp_kses( tif_memory_usage(), tif_allowed_html() );

}
