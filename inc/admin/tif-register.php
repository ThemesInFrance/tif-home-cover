<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Home Cover Sanitize and register
 */

function tif_home_cover_sanitize( $input ) {

	// Verify that the nonce is valid.
	if ( ! isset( $_POST['tif_home_cover_nonce_field'] ) || ! wp_verify_nonce( $_POST['tif_home_cover_nonce_field'], 'tif_home_cover_action' ) ) :

		$code	 = 'nonce_unverified';
		$message = esc_html__( 'Sorry, your nonce did not verify.', 'tif-home-cover' );
		$type	 = 'error';

		return $input;

	endif;

	$code	 = 'settings_updated';
	$message = esc_html__( 'Congratulations, the data has been successfully recorded.' , 'tif-home-cover' );
	$type	 = 'updated';

	/**
	 * @link https://developer.wordpress.org/reference/functions/add_settings_error/
	 */
	add_settings_error(
		esc_attr( 'home_cover_settings_error' ),
		esc_attr( $code ),
		esc_html__( $message ),
		$type
	);

	$capability = tif_get_submenu_capability( tif_get_option( 'plugin_home_cover', 'tif_init,capabilities', 'multicheck' ) ) ;

	if ( ! current_user_can( $capability ) )
		wp_die( esc_html__( 'Unauthorized user.', 'tif-home-cover' ) );

	$new_input =
		! empty( get_option( 'tif_plugin_home_cover' ) )
		? get_option( 'tif_plugin_home_cover' )
		: tif_plugin_home_cover_setup_data() ;

	// Last Tab used
	$new_input['tif_tabs'] =
		! empty( $input['tif_tabs'] )
		? tif_sanitize_absint( $input['tif_tabs'] )
		: false ;

	// Settings
	if(  current_user_can( 'manage_options' ) ) :
		$tif_init = array(
			'enabled'				=> 'checkbox',
			'customizer_enabled'	=> 'checkbox',
			'generated'				=> 'multicheck',
			'css_enabled'			=> 'key',
			'capabilities'			=> 'multicheck',
			'custom_css'			=> 'css',
		);

		foreach ( $tif_init as $key => $value) {
			$new_input['tif_init'][$key] =
				! empty( $input['tif_init'][$key] )
				? call_user_func( 'tif_sanitize_' . $value, $input['tif_init'][$key] )
				: false ;
		}
	endif;

	return $new_input;

}
