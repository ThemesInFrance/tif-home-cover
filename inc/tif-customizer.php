<?php

if ( ! defined( 'ABSPATH' ) ) exit;

if( ! tif_get_option( 'plugin_home_cover', 'tif_init,customizer_enabled', 'checkbox' ) )
return;

/**
 * Contains methods for customizing the theme customization screen.
 *
 * @link http://codex.wordpress.org/Theme_Customization_API
 */

class Tif_Home_Cover_Customize {

	/**
	 * This hooks into 'customize_register' ( available as of WP 3.4 ) and allows
	 * you to add new sections and controls to the Theme Customize screen.
	 *
	 * Note: To enable instant preview, we have to actually write a bit of custom
	 * javascript. See live_preview() for more.
	 *
	 * @see add_action( 'customize_register',$func )
	 * @param \WP_Customize_Manager $wp_customize
	 * @link http://ottopress.com/2012/how-to-leverage-the-theme-customizer-in-your-own-themes/
	 */

	public static function register ( $wp_customize ) {

		/**
		 * Assigning plugin name
		 */
		$tif_plugin_name = 'tif_plugin_home_cover';

		// === PANEL // Home Cover PANEL ================================================

		$wp_customize->add_panel(
			'tif_plugin_home_cover_panel',
			array(
				'capabitity'		=> 'edit_theme_options',
				'priority'			=> 1040,
				'title'				=> esc_html__( 'Home Cover', 'tif-home-cover' )
			)
		);

	// ... SECTION // Home Cover settings ...........................................

		$wp_customize->add_section( 'tif_plugin_home_cover_panel_layout_section', array(
			'title'					=> esc_html__( 'Home Cover settings', 'tif-home-cover' ),
			'priority'				=> 100,
			'panel'					=> 'tif_plugin_home_cover_panel'
		) );

		// Add Setting
		// ...
		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_init][enabled]',
			array(
				'default'			=> tif_get_default( 'plugin_home_cover', 'tif_init,enabled' ),
				'capability'		=> 'edit_theme_options',
				'sanitize_callback'	=> 'sanitize_text_field',
				'type'				=> 'option',
			)
		);
		$wp_customize->add_control(
			$tif_plugin_name . '[tif_init][enabled]',
			array(
				'label'				=> esc_html__( 'Home Cover enabled', 'tif-home-cover' ),
				'section'			=> 'tif_plugin_home_cover_panel_layout_section',
				'type'				=> 'checkbox',
			)
		);
		$wp_customize->selective_refresh->add_partial(
			$tif_plugin_name . '[tif_init][enabled]',
			array(
				'selector' => '#tif-home-cover-disabled',
			)
		);

		// ... SECTION // Home Cover Layout ...........................................

		$wp_customize->add_section( 'tif_plugin_home_cover_panel_layout_section', array(
			'title'					=> esc_html__( 'Home Cover layout', 'tif-home-cover' ),
			'priority'				=> 200,
			'panel'					=> 'tif_plugin_home_cover_panel'
		) );

		if ( ! class_exists( 'Themes_In_France' ) ) {

			// Add Setting
			// ...
			$wp_customize->add_setting(
				$tif_plugin_name . '[tif_home_cover_layout][boxed_width]',
				array(
					'default'			=> tif_get_default( 'plugin_home_cover', 'tif_home_cover_layout,boxed_width', 'length' ),
					'type'				=> 'option',
					'capability'		=> 'edit_theme_options',
					'sanitize_callback'	=> 'tif_sanitize_length'
				)
			);
			$wp_customize->add_control(
				new Tif_Customize_Number_Multiple_Control(
					$wp_customize,
					$tif_plugin_name . '[tif_home_cover_layout][boxed_width]',
					array(
						'section'			=> 'tif_plugin_home_cover_panel_layout_section',
						'label'				=> esc_html__( 'Boxed width', 'tif-home-cover' ),
						'choices'			=> array(
							'value'				=> esc_html__( 'Value', 'tif-home-cover' ),
							'unit'				=> esc_html__( 'Unit', 'tif-home-cover' ),
						),
						'input_attrs'		=> array(
							'min'				=> '800',
							// 'max'				=> '50',
							'step'				=> '1',
							'unit'				=> array(
								'px'				=> esc_html__( 'px', 'tif-home-cover' ),
							),
							'alignment'			=> 'column',
						),
					)
				)
			);

		}

		$wp_customize->add_setting(
			'tif_plugin_home_cover_panel_layout_section_layout_heading',
			array(
				'sanitize_callback'	=> 'tif_sanitize_key'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Heading_Control(
				$wp_customize,
				'tif_plugin_home_cover_panel_layout_section_layout_heading',
				array(
					'section'		=> 'tif_plugin_home_cover_panel_layout_section',
					'label'			=> esc_html__( 'Layout', 'tif-home-cover' ),
				)
			)
		);

		$tif_home_cover_layout = tif_get_default( 'plugin_home_cover', 'tif_home_cover_layout,layout', 'multicheck' );
		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_home_cover_layout][layout]',
			array(
				'default'			=> (array)$tif_home_cover_layout,
				'type'				=> 'option',
				'capability'		=> 'edit_theme_options',
				'sanitize_callback'	=> 'tif_sanitize_multicheck'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Wrap_Attr_Main_Layout_Control(
				$wp_customize,
				$tif_plugin_name . '[tif_home_cover_layout][layout]',
				array(
					'section'			=> 'tif_plugin_home_cover_panel_layout_section',
					'label'				=> esc_html__( 'Post Cover layout', 'tif-home-cover' ),
					'choices'           => array(
						0	=> array(
							'id'         => 'layout',
							'label'         => false,
							'choices'			=> array(
								'cover'				=> array( TIF_HOME_COVER_ADMIN_IMAGES_URL . '/layout-cover.png', esc_html_x( 'Default', 'Cover layout', 'tif-home-cover' ) ),
								// 'cover_grid_2'		=> array( TIF_HOME_COVER_ADMIN_IMAGES_URL . '/layout-cover-grid-2.png', esc_html_x( 'Cover (50/50)', 'Cover layout', 'tif-home-cover' ) ),
								'cover_media_text'	=> array( TIF_HOME_COVER_ADMIN_IMAGES_URL . '/layout-cover-media-text.png', esc_html_x( 'Media/Text', 'Cover layout', 'tif-home-cover' ) ),
								'cover_text_media'	=> array( TIF_HOME_COVER_ADMIN_IMAGES_URL . '/layout-cover-text-media.png', esc_html_x( 'Text/Media', 'Cover layout', 'tif-home-cover' ) ),
								// 'none'				=> array( TIF_HOME_COVER_ADMIN_IMAGES_URL . '/layout-none-square.png', esc_html_x( 'None', 'Cover layout', 'tif-home-cover' ) )
							),
						),
						1	=> array(
							'id'          => 'container_class',
							'description' => sprintf( '%s "%s"',
								esc_html__( 'Default value:', 'tif-home-cover' ),
								esc_html( isset( $tif_home_cover_layout[1] ) ? $tif_home_cover_layout[1] : null ),
							)
						),
						2	=> array(
							'id'          => 'post_class',
							'description' => sprintf( '%s "%s"',
								esc_html__( 'Default value:', 'tif-home-cover' ),
								esc_html( isset( $tif_home_cover_layout[2] ) ? $tif_home_cover_layout[2] : null ),
							)
						),
					),
					'settings'			=> $tif_plugin_name . '[tif_home_cover_layout][layout]'
				)
			)
		);

		$wp_customize->add_setting(
			'tif_plugin_home_cover_panel_layout_section_images_heading',
			array(
				'sanitize_callback' => 'tif_sanitize_key'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Heading_Control(
				$wp_customize,
				'tif_plugin_home_cover_panel_layout_section_images_heading',
				array(
					'section'       => 'tif_plugin_home_cover_panel_layout_section',
					'label'         => esc_html__( 'Images', 'tif-home-cover' ),
				)
			)
		);

		// Add Setting
		// ...
		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_home_cover_layout][wide_alignment]',
			array(
				'default'			=> tif_get_default( 'plugin_home_cover', 'tif_home_cover_layout,wide_alignment', 'key' ),
				'type'				=> 'option',
				'capability'		=> 'edit_theme_options',
				'sanitize_callback'	=> 'tif_sanitize_key'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Radio_Image_Control(
				$wp_customize,
				$tif_plugin_name . '[tif_home_cover_layout][wide_alignment]',
				array(
					'section'			=> 'tif_plugin_home_cover_panel_layout_section',
					'label'				=> esc_html__( 'Alignment', 'tif-home-cover' ),
					'choices'			=> array(
						'tif_boxed'			=> array( TIF_HOME_COVER_ADMIN_IMAGES_URL . '/layout-align-center.png', esc_html__( 'Align center', 'tif-home-cover' ) ),
						'alignwide'			=> array( TIF_HOME_COVER_ADMIN_IMAGES_URL . '/layout-align-wide.png', esc_html__( 'Wide width', 'tif-home-cover' ) ),
						'alignfull'			=> array( TIF_HOME_COVER_ADMIN_IMAGES_URL . '/layout-align-full.png', esc_html__( 'Full width', 'tif-home-cover' ) ),
					),
					'settings'		=> $tif_plugin_name . '[tif_home_cover_layout][wide_alignment]',
				)
			)
		);

		// Add Setting
		// ...
		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_home_cover_layout][cover_fit]',
			array(
				'default'			=> tif_get_default( 'plugin_home_cover', 'tif_home_cover_layout,cover_fit', 'cover_fit' ),
				'type'				=> 'option',
				'capability'		=> 'edit_theme_options',
				'sanitize_callback'	=> 'tif_sanitize_cover_fit'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Image_Cover_Fit_Control(
				$wp_customize,
				$tif_plugin_name . '[tif_home_cover_layout][cover_fit]',
				array(
					'section'			=> 'tif_plugin_home_cover_panel_layout_section',
					// 'label'				=> esc_html__( 'Change thumbnail alignment', 'tif-home-cover' ),
					'description'		=> array(
						'main'				=> false,
						'preset'			=> false,
						'position'			=> false,
						'height'			=> esc_html( '0 to use original ratio', 'tif-slider' ),
					),
					'choices'			=> array(
						'default'			=> esc_html__( 'Original', 'tif-home-cover' ),
						'cover'				=> esc_html__( 'Fill element', 'tif-home-cover' ),
						// 'contain'			=> esc_html__( 'Maintain ratio', 'tif-home-cover' ),
					),
					'input_attrs'		=> array(
						'min'				=> '0',
						'step'				=> '1',
						'unit'				=> array(
							'px'				=> esc_html__( 'px', 'tif-home-cover' ),
							// 'rem'				=> esc_html__( 'rem', 'tif-home-cover' ),
							'vh'				=> esc_html__( 'vh', 'tif-home-cover' ),
							'%'					=> esc_html__( '%', 'tif-home-cover' ),
						),
						'alignment'			=> 'row',
					),
					'settings'		=> $tif_plugin_name . '[tif_home_cover_layout][cover_fit]',
				)
			)
		);

		$wp_customize->add_setting(
			'tif_plugin_home_cover_panel_layout_section_alignment_heading',
			array(
				'sanitize_callback'	=> 'tif_sanitize_key'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Heading_Control(
				$wp_customize,
				'tif_plugin_home_cover_panel_layout_section_alignment_heading',
				array(
					'section'			=> 'tif_plugin_home_cover_panel_layout_section',
					'label'				=> esc_html__( 'Box Alignment', 'tif-home-cover' ),
				)
			)
		);

		$tif_home_cover_alignment = tif_get_default( 'plugin_home_cover', 'tif_home_cover_layout,alignment', 'array' );

		// Add Setting
		// ...
		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_home_cover_layout][alignment][align_items]',
			array(
				'default'			=> (string)$tif_home_cover_alignment['align_items'],
				'type'				=> 'option',
				'sanitize_callback'	=> 'tif_sanitize_key'
			)
		);
		$wp_customize->add_control(
			$tif_plugin_name . '[tif_home_cover_layout][alignment][align_items]',
			array(
				'section'			=> 'tif_plugin_home_cover_panel_layout_section',
				'label'				=> esc_html__( 'Horizontal alignment', 'tif-home-cover' ),
				'type'				=> 'select',
				'choices'			=> array(
					'' 					=> esc_html__( 'Undefined', 'tif-home-cover' ),
					'start' 			=> esc_html__( '"start"', 'tif-home-cover' ),
					'center' 			=> esc_html__( '"center"', 'tif-home-cover' ),
					'end' 				=> esc_html__( '"end"', 'tif-home-cover' ),
				),
				'settings'			=> $tif_plugin_name . '[tif_home_cover_layout][alignment][align_items]'
			)
		);

		// Add Setting
		// ...
		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_home_cover_layout][alignment][justify_content]',
			array(
				'default'			=> (string)$tif_home_cover_alignment['justify_content'],
				'type'				=> 'option',
				'sanitize_callback'	=> 'tif_sanitize_key'
			)
		);
		$wp_customize->add_control(
			$tif_plugin_name . '[tif_home_cover_layout][alignment][justify_content]',
			array(
				'section'			=> 'tif_plugin_home_cover_panel_layout_section',
				'label'				=> esc_html__( 'Vertical alignment', 'tif-home-cover' ),
				'type'				=> 'select',
				'choices'			=> tif_get_justify_content_options(),
				'settings'			=> 'tif_plugin_home_cover[tif_home_cover_layout][alignment][justify_content]'
			)
		);

		// Add Setting
		// ...
		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_home_cover_layout][alignment][gap]',
			array(
				'default'			=> (string)$tif_home_cover_alignment['gap'],
				'type'				=> 'option',
				'sanitize_callback'	=> 'tif_sanitize_key'
			)
		);
		$wp_customize->add_control(
			$tif_plugin_name . '[tif_home_cover_layout][alignment][gap]',
			array(
				'section'			=> 'tif_plugin_home_cover_panel_layout_section',
				'label'				=> esc_html__( 'Vertical spacing', 'tif-home-cover' ),
				'description'		=> esc_html__( 'Defines the vertical space between the elements.', 'tif-home-cover' ),
				'type'				=> 'select',
				'choices'			=> tif_get_gap_array( 'label' ),
				'settings'			=> 'tif_plugin_home_cover[tif_home_cover_layout][alignment][gap]'
			)
		);

		$wp_customize->add_setting(
			'tif_plugin_home_cover_panel_layout_section_box_heading',
			array(
				'sanitize_callback'	=> 'tif_sanitize_key'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Heading_Control(
				$wp_customize,
				'tif_plugin_home_cover_panel_layout_section_box_heading',
				array(
					'section'			=> 'tif_plugin_home_cover_panel_layout_section',
					'label'				=> esc_html__( 'Box Alignment', 'tif-home-cover' ),
				)
			)
		);

		$tif_home_cover_box = tif_get_default( 'plugin_home_cover', 'tif_home_cover_layout,box', 'array' );

		// Add Setting
		// ...
		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_home_cover_layout][box][padding]',
			array(
				'default'			=> tif_sanitize_scss_variables( $tif_home_cover_box['padding'] ),
				'type'				=> 'option',
				'capability'		=> 'edit_theme_options',
				'sanitize_callback'	=> 'tif_sanitize_scss_variables'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Select_Multiple_Control(
				$wp_customize,
				$tif_plugin_name . '[tif_home_cover_layout][box][padding]',
				array(
					'section'			=> 'tif_plugin_home_cover_panel_layout_section',
					'label'				=> esc_html__( 'Padding', 'tif-home-cover' ),
					'choices'			=> array(
						0					=> array(
							'label'				=> esc_html__( 'Vertical', 'tif-home-cover' ),
							'choices'			=> tif_get_spacers_array( 'label' )
						),
						1					=> array(
							'label'				=> esc_html__( 'Horizontal', 'tif-home-cover' ),
							'choices'			=> tif_get_spacers_array( 'label' )
						)
					),
					'input_attrs'		=> array(
						'alignment'			=> 'row'
					),
				)
			)
		);

		// ... SECTION // Home Cover colors ...........................................

		$wp_customize->add_section( 'tif_plugin_home_cover_panel_colors_section', array(
			'title'					=> esc_html__( 'Home Cover colors', 'tif-home-cover' ),
			'priority'				=> 200,
			'panel'					=> 'tif_plugin_home_cover_panel'
		) );

		$wp_customize->add_setting(
			'tif_plugin_home_cover_panel_colors_section_background_heading',
			array(
				'sanitize_callback' => 'tif_sanitize_key'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Heading_Control(
				$wp_customize,
				'tif_plugin_home_cover_panel_colors_section_background_heading',
				array(
					'section'       => 'tif_plugin_home_cover_panel_colors_section',
					'label'         => esc_html__( 'Background colors', 'tif-home-cover' ),
				)
			)
		);

		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_home_cover_colors][bgcolor]',
			array(
				'default'			=> tif_get_default( 'plugin_home_cover', 'tif_home_cover_colors,bgcolor', 'array_keycolor' ),
				'type'				=> 'option',
				'capability'		=> 'edit_theme_options',
				'sanitize_callback'	=> 'tif_sanitize_array_keycolor'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Color_Control(
				$wp_customize, $tif_plugin_name . '[tif_home_cover_colors][bgcolor]',
				array(
					'section'		=> 'tif_plugin_home_cover_panel_colors_section',
					'label'			=> esc_html__( 'Background', 'tif-home-cover' ),
					'choices'			=> tif_get_theme_hex_colors(),
					'input_attrs'		=> array(
						'format'			=> 'hex',	// key, hex
						// 'tif'				=> 'key',	// switch to key if class_exists ( 'Themes_In_France' )
						'output'			=> 'array',
						'brightness'		=> false,
						'opacity'			=> false,
					),
				)
			)
		);

		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_home_cover_colors][overlay_bgcolor]',
			array(
				'default'			=> tif_get_default( 'plugin_home_cover', 'tif_home_cover_colors,overlay_bgcolor', 'array_keycolor' ),
				'type'				=> 'option',
				'capability'		=> 'edit_theme_options',
				'sanitize_callback'	=> 'tif_sanitize_array_keycolor'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Color_Control(
				$wp_customize, $tif_plugin_name . '[tif_home_cover_colors][overlay_bgcolor]',
				array(
					'section'		=> 'tif_plugin_home_cover_panel_colors_section',
					'label'			=> esc_html__( 'Overlay', 'tif-home-cover' ),
					'choices'			=> tif_get_theme_hex_colors(),
					'input_attrs'		=> array(
						'format'			=> 'hex',	// key, hex
						// 'tif'				=> 'key',	// switch to key if class_exists ( 'Themes_In_France' )
						'output'			=> 'array',
						'brightness'		=> false,
						'opacity'			=> true,
					),
				)
			)
		);

		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_home_cover_colors][title_bgcolor]',
			array(
				'default'			=> tif_get_default( 'plugin_home_cover', 'tif_home_cover_colors,title_bgcolor', 'array_keycolor' ),
				'type'				=> 'option',
				'capability'		=> 'edit_theme_options',
				'sanitize_callback'	=> 'tif_sanitize_array_keycolor'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Color_Control(
				$wp_customize, $tif_plugin_name . '[tif_home_cover_colors][title_bgcolor]',
				array(
					'section'		=> 'tif_plugin_home_cover_panel_colors_section',
					'label'			=> esc_html__( 'Title', 'tif-home-cover' ),
					'choices'			=> tif_get_theme_hex_colors(),
					'input_attrs'		=> array(
						'format'			=> 'hex',	// key, hex
						// 'tif'				=> 'key',	// switch to key if class_exists ( 'Themes_In_France' )
						'output'			=> 'array',
						'brightness'		=> false,
						'opacity'			=> true,
					),
				)
			)
		);

		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_home_cover_colors][subtitle_bgcolor]',
			array(
				'default'			=> tif_get_default( 'plugin_home_cover', 'tif_home_cover_colors,subtitle_bgcolor', 'array_keycolor' ),
				'type'				=> 'option',
				'capability'		=> 'edit_theme_options',
				'sanitize_callback'	=> 'tif_sanitize_array_keycolor'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Color_Control(
				$wp_customize, $tif_plugin_name . '[tif_home_cover_colors][subtitle_bgcolor]',
				array(
					'section'		=> 'tif_plugin_home_cover_panel_colors_section',
					'label'			=> esc_html__( 'Sub title', 'tif-home-cover' ),
					'choices'			=> tif_get_theme_hex_colors(),
					'input_attrs'		=> array(
						'format'			=> 'hex',	// key, hex
						// 'tif'				=> 'key',	// switch to key if class_exists ( 'Themes_In_France' )
						'output'			=> 'array',
						'brightness'		=> false,
						'opacity'			=> true,
					),
				)
			)
		);

		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_home_cover_colors][excerpt_bgcolor]',
			array(
				'default'			=> tif_get_default( 'plugin_home_cover', 'tif_home_cover_colors,excerpt_bgcolor', 'array_keycolor' ),
				'type'				=> 'option',
				'capability'		=> 'edit_theme_options',
				'sanitize_callback'	=> 'tif_sanitize_array_keycolor'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Color_Control(
				$wp_customize, $tif_plugin_name . '[tif_home_cover_colors][excerpt_bgcolor]',
				array(
					'section'		=> 'tif_plugin_home_cover_panel_colors_section',
					'label'			=> esc_html__( 'Excerpt', 'tif-home-cover' ),
					'choices'			=> tif_get_theme_hex_colors(),
					'input_attrs'		=> array(
						'format'			=> 'hex',	// key, hex
						// 'tif'				=> 'key',	// switch to key if class_exists ( 'Themes_In_France' )
						'output'			=> 'array',
						'brightness'		=> false,
						'opacity'			=> true,
					),
				)
			)
		);

		$wp_customize->add_setting(
			'tif_plugin_home_cover_panel_colors_section_text_heading',
			array(
				'sanitize_callback' => 'tif_sanitize_key'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Heading_Control(
				$wp_customize,
				'tif_plugin_home_cover_panel_colors_section_text_heading',
				array(
					'section'       => 'tif_plugin_home_cover_panel_colors_section',
					'label'         => esc_html__( 'Text colors', 'tif-home-cover' ),
				)
			)
		);

		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_home_cover_colors][title_color]',
			array(
				'default'			=> tif_get_default( 'plugin_home_cover', 'tif_home_cover_colors,title_color', 'array_keycolor' ),
				'type'				=> 'option',
				'capability'		=> 'edit_theme_options',
				'sanitize_callback'	=> 'tif_sanitize_array_keycolor'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Color_Control(
				$wp_customize, $tif_plugin_name . '[tif_home_cover_colors][title_color]',
				array(
					'section'		=> 'tif_plugin_home_cover_panel_colors_section',
					'label'			=> esc_html__( 'Title', 'tif-home-cover' ),
					'choices'			=> tif_get_theme_hex_colors(),
					'input_attrs'		=> array(
						'format'			=> 'hex',	// key, hex
						// 'tif'				=> 'key',	// switch to key if class_exists ( 'Themes_In_France' )
						'output'			=> 'array',
						'brightness'		=> false,
						'opacity'			=> false,
					),
				)
			)
		);

		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_home_cover_colors][subtitle_color]',
			array(
				'default'			=> tif_get_default( 'plugin_home_cover', 'tif_home_cover_colors,subtitle_color', 'array_keycolor' ),
				'type'				=> 'option',
				'capability'		=> 'edit_theme_options',
				'sanitize_callback'	=> 'tif_sanitize_array_keycolor'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Color_Control(
				$wp_customize, $tif_plugin_name . '[tif_home_cover_colors][subtitle_color]',
				array(
					'section'		=> 'tif_plugin_home_cover_panel_colors_section',
					'label'			=> esc_html__( 'Sub title', 'tif-home-cover' ),
					'choices'			=> tif_get_theme_hex_colors(),
					'input_attrs'		=> array(
						'format'			=> 'hex',	// key, hex
						// 'tif'				=> 'key',	// switch to key if class_exists ( 'Themes_In_France' )
						'output'			=> 'array',
						'brightness'		=> false,
						'opacity'			=> false,
					),
				)
			)
		);

		$wp_customize->add_setting(
			$tif_plugin_name . '[tif_home_cover_colors][excerpt_color]',
			array(
				'default'			=> tif_get_default( 'plugin_home_cover', 'tif_home_cover_colors,excerpt_color', 'array_keycolor' ),
				'type'				=> 'option',
				'capability'		=> 'edit_theme_options',
				'sanitize_callback'	=> 'tif_sanitize_array_keycolor'
			)
		);
		$wp_customize->add_control(
			new Tif_Customize_Color_Control(
				$wp_customize, $tif_plugin_name . '[tif_home_cover_colors][excerpt_color]',
				array(
					'section'		=> 'tif_plugin_home_cover_panel_colors_section',
					'label'			=> esc_html__( 'Excerpt', 'tif-home-cover' ),
					'choices'			=> tif_get_theme_hex_colors(),
					'input_attrs'		=> array(
						'format'			=> 'hex',	// key, hex
						// 'tif'				=> 'key',	// switch to key if class_exists ( 'Themes_In_France' )
						'output'			=> 'array',
						'brightness'		=> false,
						'opacity'			=> false,
					),
				)
			)
		);

		// ... SECTION // Home Cover Content ..........................................

		for ( $i = 1; $i <= 1; $i++ ) {

			$tif_home_cover[$i] = tif_get_default( 'plugin_home_cover', 'tif_home_cover_content,cover_' . $i, 'array' );

			$wp_customize->add_section( 'tif_plugin_home_cover_panel_cover_' . (int)$i . '_section' ,
			array(
				'title'					=> esc_html__( 'Home Cover content', 'tif-home-cover' ),
				'panel'					=> 'tif_plugin_home_cover_panel',
				'priority'				=> 300,
				)
			);

			// Add Setting
			// ...
			$wp_customize->add_setting(
				$tif_plugin_name . '[tif_home_cover_content][cover_' . $i . '][thumbnail_id]',
				array(
					'type'				=> 'option',
					'capability'		=> 'edit_theme_options',
					'sanitize_callback'	=> 'absint'
				)
			);
			$wp_customize->add_control(
				// new WP_Customize_Image_Control(	// url
				new WP_Customize_Media_Control(		// media id
					$wp_customize,
					$tif_plugin_name . '[tif_home_cover_content][cover_' . $i . '][thumbnail_id]',
					array(
						'section'		=> 'tif_plugin_home_cover_panel_cover_' . (int)$i . '_section',
						'label'			=> esc_html__( 'Change the default thumbnail', 'tif-home-cover' ),
						'setting'		=> $tif_plugin_name . '[tif_home_cover_content][cover_' . $i . '][thumbnail_id]'
					)
				)
			);

			// Add Setting
			// ...
			$wp_customize->add_setting(
				$tif_plugin_name . '[tif_home_cover_content][cover_' . $i . '][link]',
				array(
					'default'			=> esc_url_raw( $tif_home_cover[$i]['link'] ),
					'capability'		=> 'edit_theme_options',
					'sanitize_callback'	=> 'sanitize_url',
					'type'				=> 'option'
				)
			);
			$wp_customize->add_control(
				$tif_plugin_name . '[tif_home_cover_content][cover_' . $i . '][link]',
				array(
					'label'				=> esc_html__( 'Link', 'tif-home-cover' ),
					'section'			=> 'tif_plugin_home_cover_panel_cover_' . (int)$i . '_section',
					'type'				=> 'url',
				)
			);

			$wp_customize->add_setting(
				'tif_plugin_home_cover_panel_cover_' . (int)$i . '_section_title_heading',
				array(
					'sanitize_callback'	=> 'tif_sanitize_key'
				)
			);
			$wp_customize->add_control(
				new Tif_Customize_Heading_Control(
					$wp_customize,
					'tif_plugin_home_cover_panel_cover_' . (int)$i . '_section_title_heading',
					array(
						'section'			=> 'tif_plugin_home_cover_panel_cover_' . (int)$i . '_section',
						'label'				=> esc_html__( 'Title', 'tif-home-cover' ),
					)
				)
			);

			$tif_home_cover_title[$i] = tif_sanitize_wrap_attr( $tif_home_cover[$i]['title'] );
			$wp_customize->add_setting(
				$tif_plugin_name . '[tif_home_cover_content][cover_' . $i . '][title]',
				array(
					'default'			=> (array)$tif_home_cover_title[$i],
					'type'				=> 'option',
					'capability'		=> 'edit_theme_options',
					'sanitize_callback'	=> 'tif_sanitize_multicheck'
				)
			);
			$wp_customize->add_control(
				new Tif_Customize_Wrap_Attr_Content_Control(
					$wp_customize,
					$tif_plugin_name . '[tif_home_cover_content][cover_' . $i . '][title]',
					array(
						'section'			=> 'tif_plugin_home_cover_panel_cover_' . (int)$i . '_section',
						// 'label'				=> esc_html__( 'Title', 'tif-home-cover' ),
						'choices'           => array(
							0	=> array(
								'id'          => 'content',
								'type'        => 'text',
								'description' => sprintf( '%s "%s"',
									esc_html__( 'Default value:', 'tif-home-cover' ),
									esc_html( isset( $tif_home_cover_title[$i][0] ) ? $tif_home_cover_title[$i][0] : null ),
								)
							),
							1	=> array(
								'id'          => 'container_tag',
								'description' => sprintf( '%s "%s"',
									esc_html__( 'Default value:', 'tif-home-cover' ),
									esc_html( isset( $tif_home_cover_title[$i][1] ) ? $tif_home_cover_title[$i][1] : null ),
								)
							),
							2	=> array(
								'id'          => 'container_class',
								'description' => sprintf( '%s "%s"',
									esc_html__( 'Default value:', 'tif-home-cover' ),
									esc_html( isset( $tif_home_cover_title[$i][2] ) ? $tif_home_cover_title[$i][2] : null ),
								)
							),
						),
						'settings'			=> $tif_plugin_name . '[tif_home_cover_content][cover_' . $i . '][title]'
					)
				)
			);

			$wp_customize->add_setting(
				'tif_plugin_home_cover_panel_cover_' . (int)$i . '_section_sub_title_heading',
				array(
					'sanitize_callback'	=> 'tif_sanitize_key'
				)
			);
			$wp_customize->add_control(
				new Tif_Customize_Heading_Control(
					$wp_customize,
					'tif_plugin_home_cover_panel_cover_' . (int)$i . '_section_sub_title_heading',
					array(
						'section'			=> 'tif_plugin_home_cover_panel_cover_' . (int)$i . '_section',
						'label'				=> esc_html__( 'Sub title', 'tif-home-cover' ),
					)
				)
			);

			$tif_home_cover_subtitle[$i] = tif_sanitize_wrap_attr( $tif_home_cover[$i]['subtitle'] );
			$wp_customize->add_setting(
				$tif_plugin_name . '[tif_home_cover_content][cover_' . $i . '][subtitle]',
				array(
					'default'			=> (array)$tif_home_cover_subtitle[$i],
					'type'				=> 'option',
					'capability'		=> 'edit_theme_options',
					'sanitize_callback'	=> 'tif_sanitize_multicheck'
				)
			);
			$wp_customize->add_control(
				new Tif_Customize_Wrap_Attr_Content_Control(
					$wp_customize,
					$tif_plugin_name . '[tif_home_cover_content][cover_' . $i . '][subtitle]',
					array(
						'section'			=> 'tif_plugin_home_cover_panel_cover_' . (int)$i . '_section',
						// 'label'				=> esc_html__( 'Sub title', 'tif-home-cover' ),
						'choices'           => array(
							0	=> array(
								'id'          => 'content',
								'type'        => 'text',
								'description' => sprintf( '%s "%s"',
									esc_html__( 'Default value:', 'tif-home-cover' ),
									esc_html( isset( $tif_home_cover_subtitle[$i][0] ) ? $tif_home_cover_subtitle[$i][0] : null ),
								)
							),
							1	=> array(
								'id'          => 'container_tag',
								'description' => sprintf( '%s "%s"',
									esc_html__( 'Default value:', 'tif-home-cover' ),
									esc_html( isset( $tif_home_cover_subtitle[$i][1] ) ? $tif_home_cover_subtitle[$i][1] : null ),
								)
							),
							2	=> array(
								'id'          => 'container_class',
								'description' => sprintf( '%s "%s"',
									esc_html__( 'Default value:', 'tif-home-cover' ),
									esc_html( isset( $tif_home_cover_subtitle[$i][2] ) ? $tif_home_cover_subtitle[$i][2] : null ),
								)
							),
						),
						'settings'			=> $tif_plugin_name . '[tif_home_cover_content][cover_' . $i . '][subtitle]'
					)
				)
			);

			// Add Setting
			// ...
			$wp_customize->add_setting(
				'tif_plugin_home_cover_panel_cover_' . (int)$i . '_section_excerpt_heading',
				array(
					'sanitize_callback'	=> 'tif_sanitize_key'
				)
			);
			$wp_customize->add_control(
				new Tif_Customize_Heading_Control(
					$wp_customize,
					'tif_plugin_home_cover_panel_cover_' . (int)$i . '_section_excerpt_heading',
					array(
						'section'			=> 'tif_plugin_home_cover_panel_cover_' . (int)$i . '_section',
						'label'				=> esc_html__( 'Description', 'tif-home-cover' ),
					)
				)
			);

			$tif_home_cover_excerpt[$i] = tif_sanitize_wrap_attr( $tif_home_cover[$i]['excerpt'] );
			$wp_customize->add_setting(
				$tif_plugin_name . '[tif_home_cover_content][cover_' . $i . '][excerpt]',
				array(
					'default'			=> (array)$tif_home_cover_excerpt[$i],
					'type'				=> 'option',
					'capability'		=> 'edit_theme_options',
					'sanitize_callback'	=> 'tif_sanitize_multicheck'
				)
			);
			$wp_customize->add_control(
				new Tif_Customize_Wrap_Attr_Content_Control(
					$wp_customize,
					$tif_plugin_name . '[tif_home_cover_content][cover_' . $i . '][excerpt]',
					array(
						'section'			=> 'tif_plugin_home_cover_panel_cover_' . (int)$i . '_section',
						// 'label'				=> esc_html__( 'Description for this slide', 'tif-home_cover' ),
						'choices'           => array(
							0	=> array(
								'id'          => 'content',
								'type'        => 'textarea',
								'description' => sprintf( '%s "%s"',
									esc_html__( 'Default value:', 'tif-home-cover' ),
									esc_html( isset( $tif_home_cover_excerpt[$i][0] ) ? $tif_home_cover_excerpt[$i][0] : null ),
								)
							),
							1	=> array(
								'id'          => 'container_tag',
								'description' => sprintf( '%s "%s"',
									esc_html__( 'Default value:', 'tif-home-cover' ),
									esc_html( isset( $tif_home_cover_excerpt[$i][1] ) ? $tif_home_cover_excerpt[$i][1] : null ),
								)
							),
							2	=> array(
								'id'          => 'container_class',
								'description' => sprintf( '%s "%s"',
									esc_html__( 'Default value:', 'tif-home-cover' ),
									esc_html( isset( $tif_home_cover_excerpt[$i][2] ) ? $tif_home_cover_excerpt[$i][2] : null ),
								)
							),
						),
						'settings'			=> $tif_plugin_name . '[tif_home_cover_content][cover_' . $i . '][excerpt]'
					)
				)
			);

		// ... SECTION // Home Cover Buttons ..............................................


			for ( $j = 1; $j <= 2; $j++ ) {

				$wp_customize->add_setting(
					'tif_plugin_home_cover_panel_cover_' . (int)$i . '_section_button' . $j . '_heading',
					array(
						'sanitize_callback'	=> 'tif_sanitize_key'
					)
				);
				$wp_customize->add_control(
					new Tif_Customize_Heading_Control(
						$wp_customize,
						'tif_plugin_home_cover_panel_cover_' . (int)$i . '_section_button' . $j . '_heading',
						array(
							'section'			=> 'tif_plugin_home_cover_panel_cover_' . (int)$i . '_section',
							'label'				=> sprintf( esc_html__( 'Button #%1$s', 'tif-home-cover' ), $j ),
						)
					)
				);

				// Add Setting
				// ...
				$wp_customize->add_setting(
					$tif_plugin_name . '[tif_home_cover_content][cover_' . $i . '][btn' . $j . '_enabled]',
					array(
						'default'			=> $tif_home_cover[$i]['btn' . $j . '_enabled'],
						'capability'		=> 'edit_theme_options',
						'sanitize_callback'	=> 'sanitize_text_field',
						'type'				=> 'option',
					)
				);
				$wp_customize->add_control(
					$tif_plugin_name . '[tif_home_cover_content][cover_' . $i . '][btn' . $j . '_enabled]',
					array(
						'label'				=> sprintf( esc_html__( 'Home Cover Button #%1$s enabled', 'tif-home-cover' ), $j ),
						'section'			=> 'tif_plugin_home_cover_panel_cover_' . (int)$i . '_section',
						'type'				=> 'checkbox',
					)
				);

				$wp_customize ->add_setting (
					$tif_plugin_name . '[tif_home_cover_content][cover_' . $i . '][btn' . $j . '_text]',
					array(
						'default'			=> $tif_home_cover[$i]['btn' . $j . '_text'],
						'capability'		=> 'edit_theme_options',
						'sanitize_callback'	=> 'sanitize_text_field',
						'type'				=> 'option',
					)
				);
				$wp_customize->add_control (
					$tif_plugin_name . '[tif_home_cover_content][cover_' . $i . '][btn' . $j . '_text]',
					array (
						'label'				=> esc_html__( 'Button text', 'tif-home-cover' ),
						'section'			=> 'tif_plugin_home_cover_panel_cover_' . (int)$i . '_section',
						'type'				=> 'text',
					)
				);

				$wp_customize ->add_setting (
					$tif_plugin_name . '[tif_home_cover_content][cover_' . $i . '][btn' . $j . '_link]',
					array(
						'default'			=> $tif_home_cover[$i]['btn' . $j . '_link'],
						'capability'		=> 'edit_theme_options',
						'sanitize_callback'	=> 'sanitize_url',
						'type'				=> 'option',
					)
				);
				$wp_customize->add_control (
					$tif_plugin_name . '[tif_home_cover_content][cover_' . $i . '][btn' . $j . '_link]',
					array (
						'label'				=> esc_html__( 'Button Link', 'tif-home-cover' ),
						'section'			=> 'tif_plugin_home_cover_panel_cover_' . (int)$i . '_section',
						'type'				=> 'url',
					)
				);

				// Add Setting
				// ...
				$wp_customize->add_setting(
					$tif_plugin_name . '[tif_home_cover_content][cover_' . $i . '][btn' . $j . '_link_target]',
					array(
						'default'			=> $tif_home_cover[$i]['btn' . $j . '_link_target'],
						'capability'		=> 'edit_theme_options',
						'sanitize_callback'	=> 'sanitize_text_field',
						'type'				=> 'option',
					)
				);
				$wp_customize->add_control(
					$tif_plugin_name . '[tif_home_cover_content][cover_' . $i . '][btn' . $j . '_link_target]',
					array(
						'type'				=> 'checkbox',
						'label'				=> esc_html__( 'Open link new tab', 'tif-home-cover' ),
						'section'			=> 'tif_plugin_home_cover_panel_cover_' . (int)$i . '_section',
					)
				);

				// Add Setting
				// ...
				$wp_customize->add_setting(
					$tif_plugin_name . '[tif_home_cover_content][cover_' . $i . '][btn' . $j . '_style]',
					array(
						'default'			=> $tif_home_cover[$i]['btn' . $j . '_style'],
						'type'				=> 'option',
						'sanitize_callback'	=> 'sanitize_text_field',
					)
				);
				$wp_customize->add_control(
					$tif_plugin_name . '[tif_home_cover_content][cover_' . $i . '][btn' . $j . '_style]',
					array(
						'type'				=> 'select',
						'label'				=> esc_html__( 'Button Styles', 'tif-home-cover' ),
						'section'			=> 'tif_plugin_home_cover_panel_cover_' . (int)$i . '_section',
						'choices'			=> array(
							'btn--default btn-default'		=> esc_html__( 'Defaut', 'tif-home-cover' ),
							'btn--primary btn-primary'		=> esc_html__( 'Primary', 'tif-home-cover' ),
							'btn--success btn-success'		=> esc_html__( 'Success', 'tif-home-cover' ),
							'btn--info btn-info'			=> esc_html__( 'Info', 'tif-home-cover' ),
							'btn--warning btn-warning'		=> esc_html__( 'Warning', 'tif-home-cover' ),
							'btn--danger btn-danger'		=> esc_html__( 'Danger', 'tif-home-cover' ),
						)
					)
				);

			}

		}

	}

}

// Setup the Theme Customizer settings and controls...
add_action( 'customize_register' , array( 'Tif_Home_Cover_Customize' , 'register' ) );
